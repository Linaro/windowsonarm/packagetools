import argparse
import json
import logging
import subprocess
import sys
from dataclasses import dataclass
from os.path import dirname, exists, join
from pathlib import Path

sys.path.append(dirname(dirname(__file__)))


@dataclass
class Package:
    data: str
    id: str
    release_name: str


def parse_args():
    from builder import PACKAGE_DEST_DIR

    desc = "Upload package as an Azure Blob"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(
        "--package-directory",
        default=PACKAGE_DEST_DIR,
        help="Folder where package was created (default: " + PACKAGE_DEST_DIR + ")",
    )
    parser.add_argument(
        "--ignore-if-package-is-missing",
        default=False,
        action="store_true",
        help="If package directory is not found, exit with error code 0",
    )
    parser.add_argument(
        "--azure-account-name",
        required=True,
    )
    parser.add_argument(
        "--azure-account-key",
        required=True,
    )
    parser.add_argument(
        "--azure-container-name",
        required=True,
    )
    return parser.parse_args()


def package_data(package_dir: str) -> str:
    from builder import PACKAGE_DATA_BASENAME

    return join(package_dir, PACKAGE_DATA_BASENAME)


def package_json(package_dir: str) -> str:
    from builder import PACKAGE_JSON_BASENAME

    return join(package_dir, PACKAGE_JSON_BASENAME)


def find_package(package_dir: str) -> Package:
    from builder import PackageInfo

    data = package_data(package_dir)
    if not exists(data):
        raise Exception("can't find package data '" + data + "'")
    data_format = Path(data).suffix

    json_path = package_json(package_dir)
    if not exists(json_path):
        raise Exception("can't find package json '" + json_path + "'")

    json_content = Path(json_path).read_text()
    info = PackageInfo(**json.loads(json_content))

    release_name = info.id + "/" + info.id + "_" + info.version + data_format

    return Package(data=data, id=info.id, release_name=release_name)


def upload_package(package: Package, credentials):
    credentials = [
        "--container-name",
        credentials.azure_container_name,
        "--account-name",
        credentials.azure_account_name,
        "--account-key",
        credentials.azure_account_key,
    ]
    logging.info("upload '" + package.data + "' as " + package.release_name)
    subprocess.check_call(
        [
            "az.cmd",
            "storage",
            "blob",
            "upload",
            "--file",
            package.data,
            "--name",
            package.release_name,
            "--overwrite",
        ]
        + credentials
    )
    url = (
        subprocess.check_output(
            ["az.cmd", "storage", "blob", "url", "--name", package.release_name]
            + credentials
        )
        .decode()
        .replace('"', "")
        .strip()
    )
    logging.info("package available at: " + url)


def main():
    args = parse_args()
    if not exists(args.package_directory) and args.ignore_if_package_is_missing:
        logging.warning("no package found at " + args.package_directory + " (ignored)")
        return

    package = find_package(args.package_directory)
    upload_package(package, args)


if __name__ == "__main__":
    main()
