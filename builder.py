import dataclasses
import json
import logging
import os
import shutil
import stat
import tempfile
from abc import ABC, abstractmethod
from dataclasses import dataclass
from os.path import basename, join

logging.getLogger().setLevel(logging.INFO)

PACKAGE_DEST_DIR = "package"
PACKAGE_DATA_FORMAT = "zip"
PACKAGE_DATA_BASENAME_WITHOUT_EXT = "package-data"
PACKAGE_DATA_BASENAME = PACKAGE_DATA_BASENAME_WITHOUT_EXT + "." + PACKAGE_DATA_FORMAT
PACKAGE_JSON_BASENAME = "package.json"


@dataclass
class PackageInfo:
    description: str
    id: str
    pretty_name: str
    version: str


def fix_file_to_delete(action, name, exc):
    try:
        # try to kill matching exe file - do not kill python itself!
        if name.endswith(".exe") and basename(name) != "python.exe":
            print("Kill program: {}".format(name))
            os.system("taskkill /f /im " + basename(name))
        # set file as writable
        os.chmod(name, stat.S_IWRITE)
    except Exception as error:
        print("An error occurred: {}".format(error))


class PackageRecipe(ABC):
    @abstractmethod
    def describe(self) -> PackageInfo:
        pass

    @abstractmethod
    def checkout(self):
        pass

    @abstractmethod
    def build(self):
        pass

    @abstractmethod
    def test(self):
        pass

    @abstractmethod
    def package(self, out_dir: str):
        pass


class PackageShortRecipe(PackageRecipe):
    @abstractmethod
    def all_steps(self, out_dir: str):
        pass

    def checkout(self):
        pass

    def build(self):
        pass

    def test(self):
        pass

    def package(self, out_dir: str):
        self.all_steps(out_dir)


class PackageBuilder:
    def __init__(self, recipe: PackageRecipe):
        if not isinstance(recipe, PackageRecipe):
            raise AssertionError("recipe does not implement PackageRecipe")
        self.recipe = recipe
        self.description = self.recipe.describe()
        if not isinstance(self.description, PackageInfo):
            raise AssertionError("describe() should return a PackageInfo")

        self.working_dir = os.getcwd()

    def package_files(self, out_dir: str):
        if not os.listdir(out_dir):
            logging.info("nothing found to package")
            return

        dest_dir = join(self.working_dir, PACKAGE_DEST_DIR)
        logging.info("create output directory " + dest_dir)
        if not os.path.exists(dest_dir):
            os.mkdir(dest_dir)

        archive_format = PACKAGE_DATA_FORMAT
        out_archive = join(dest_dir, PACKAGE_DATA_BASENAME_WITHOUT_EXT)
        shutil.make_archive(out_archive, archive_format, root_dir=out_dir)
        out_archive = join(dest_dir, PACKAGE_DATA_BASENAME)
        logging.info("create data archive " + out_archive)

        out_json = join(dest_dir, PACKAGE_JSON_BASENAME)
        with open(out_json, "w") as out:
            data = dataclasses.asdict(self.description)
            json.dump(data, out, indent=4, sort_keys=True)
        logging.info("create description " + out_json)

    def make_from_current_dir(self):
        logging.info("describe")
        logging.info(self.description)
        logging.info("checkout")
        self.recipe.checkout()
        logging.info("build")
        self.recipe.build()
        logging.info("test")
        self.recipe.test()
        logging.info("package")
        with tempfile.TemporaryDirectory() as out_dir:
            self.recipe.package(out_dir)
            self.package_files(out_dir)

    def make(self):
        work_dir = os.getenv("PACKAGE_TOOLS_WORKDIR")
        if work_dir:
            work_dir = os.path.join(work_dir, "tmp")
            if os.path.exists(work_dir):
                os.chdir("C:/")
                # first kill processes and change file rights
                shutil.rmtree(work_dir, onerror=fix_file_to_delete)
                # then delete
                shutil.rmtree(work_dir, ignore_errors=True)
            os.makedirs(work_dir)
            os.chdir(work_dir)
        self.make_from_current_dir()
