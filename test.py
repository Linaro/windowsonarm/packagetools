import unittest

from builder import PackageBuilder, PackageInfo, PackageRecipe

default_package_info = PackageInfo(
    description="Nothing to be said",
    id="default_package",
    pretty_name="Default Package",
    version="0.0",
)


class DefaultRecipe(PackageRecipe):
    def describe(self) -> PackageInfo:
        return default_package_info

    def checkout(self):
        pass

    def build(self):
        pass

    def test(self):
        pass

    def package(self, out_dir: str):
        pass


class TestPackageBuilder(unittest.TestCase):
    def test_bad_recipe(self):
        class NotARealRecipe:
            def f():
                pass

        with self.assertRaises(AssertionError):
            PackageBuilder(NotARealRecipe())

    def test_make(self):
        class Recipe(PackageRecipe):
            def __init__(self):
                self.describe_called = False
                self.checkout_called = False
                self.build_called = False
                self.test_called = False
                self.package_called = False

            def describe(self) -> PackageInfo:
                self.describe_called = True
                return default_package_info

            def checkout(self):
                self.checkout_called = True

            def build(self):
                self.build_called = True

            def test(self):
                self.test_called = True

            def package(self, out_dir: str):
                self.package_called = True

        r = Recipe()
        PackageBuilder(r).make()
        self.assertTrue(r.describe_called)
        self.assertTrue(r.checkout_called)
        self.assertTrue(r.build_called)
        self.assertTrue(r.test_called)
        self.assertTrue(r.package_called)

    def test_failing_build(self):
        class FailingBuild(DefaultRecipe):
            def build(self):
                raise ValueError()

        with self.assertRaises(ValueError):
            PackageBuilder(FailingBuild()).make()


if __name__ == "__main__":
    unittest.main()
