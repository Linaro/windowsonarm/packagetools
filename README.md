Package Tools
-------------

This python module is designed to be imported by packages.

Every package defines a recipe, that implements a given interface.

Write new package
-----------------

You need to clone this current repository and it to PYTHONPATH

```
$ git clone https://gitlab.com/Linaro/windowsonarm/packagetools
$ export PYTHONPATH=$(pwd)
$ cd your_recipe
$ python recipe.py
```

To implement recipe.py, see those examples:
- [as python recipe](https://gitlab.com/Linaro/windowsonarm/packages/hello-world)
- [as bash script](https://gitlab.com/Linaro/windowsonarm/packages/hello-world-using-bash)

For CI/CD, enabling build and upload, simply create a .gitlab-ci.yml with:

```
include: 'https://gitlab.com/Linaro/windowsonarm/packagetools/-/raw/master/ci/package.yml'
```

Note: your package should be formatted correctly to be allowed to build.

To ensure this, use script from this repository: `./scripts/format`

Timeout
-------

By default, any CI job is limited to 1h. You can increase that limit for your project:

Settings -> CI/CD -> General pipelines -> Timeout

Runners and jobs are configured for 12h maximum timeout.

Dependencies
------------

Only python standard library is accepted as dependency.

It allows packages to not depend on anything beyond vanilla Python.

Environment
-----------

gitlab-runner is running under [wenv](https://gitlab.com/Linaro/windowsonarm/wenv) arm64.

You can use any dependency installed by it.

Contribution
------------

MR are mandatory on this repository, including for maintainers.

Code should be formatted, linted and tested to be accepted by CI.

To do all those steps on your (linux) machine: `./scripts/check`

Packages
--------

list of packages available from (uses [azure-blob-list](https://github.com/tylergibson/azure-blob-list)):

https://woastorage.blob.core.windows.net/packages/index.html
